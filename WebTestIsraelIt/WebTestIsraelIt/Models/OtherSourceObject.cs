﻿namespace WebTestIsraelIt.Models
{
    public class OtherSourceObject
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public long WorkExperience { get; set; }
    }
}
