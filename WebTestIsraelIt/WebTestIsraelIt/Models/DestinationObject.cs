﻿namespace WebTestIsraelIt.Models
{
    public class DestinationObject
    {
        public string Bool { get; set; }

        public string Byte { get; set; }

        public string SByte { get; set; }

        public string Short { get; set; }

        public string UShort { get; set; }

        public string Int { get; set; }

        public string UInt { get; set; }

        public string Long { get; set; }

        public string ULong { get; set; }

        public string Float { get; set; }

        public string Double { get; set; }

        public string Decimal { get; set; }

        public string Char { get; set; }

        public string String { get; set; }
    }
}
