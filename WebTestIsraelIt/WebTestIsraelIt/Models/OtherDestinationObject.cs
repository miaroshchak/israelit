﻿namespace WebTestIsraelIt.Models
{
    public class OtherDestinationObject
    {
        public string FullName {get; set;}

        public double Age { get; set; }

        public double WorkExperience { get; set; }
    }
}
