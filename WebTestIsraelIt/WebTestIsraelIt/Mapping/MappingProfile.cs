﻿using AutoMapper;
using WebTestIsraelIt.Models;

namespace WebTestIsraelIt.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {

            CreateMap<SourceObject, DestinationObject>()
                .ForMember(dest => dest.Bool, opt => opt.MapFrom(src => src.Bool.ToString()))
                .ForMember(dest => dest.Byte, opt => opt.MapFrom(src => src.Byte.ToString()))
                .ForMember(dest => dest.SByte, opt => opt.MapFrom(src => src.SByte.ToString()))
                .ForMember(dest => dest.Short, opt => opt.MapFrom(src => src.Short.ToString()))
                .ForMember(dest => dest.UShort, opt => opt.MapFrom(src => src.UShort.ToString()))
                .ForMember(dest => dest.Int, opt => opt.MapFrom(src => src.Int.ToString()))
                .ForMember(dest => dest.UInt, opt => opt.MapFrom(src => src.UInt.ToString()))
                .ForMember(dest => dest.Long, opt => opt.MapFrom(src => src.Long.ToString()))
                .ForMember(dest => dest.ULong, opt => opt.MapFrom(src => src.ULong.ToString()))
                .ForMember(dest => dest.Float, opt => opt.MapFrom(src => src.Float.ToString()))
                .ForMember(dest => dest.Double, opt => opt.MapFrom(src => src.Double.ToString()))
                .ForMember(dest => dest.Decimal, opt => opt.MapFrom(src => src.Decimal.ToString()))
                .ForMember(dest => dest.Char, src => src.MapFrom(src => src.Char.ToString()));


            CreateMap<OtherSourceObject, OtherDestinationObject>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " +src.LastName))
                .ForMember(dest => dest.Age, opt => opt.MapFrom(src => src.Age))
                .ForMember(dest => dest.WorkExperience, opt => opt.MapFrom(src => src.WorkExperience));

        }
    }
}
