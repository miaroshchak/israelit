﻿using System;
using System.Linq;
using System.Reflection;

namespace WebTestIsraelIt.Mapping
{
    public class Mapper<TSourceObject, TDestObject> where TDestObject : new()
    {
        protected void MatchField(TSourceObject source, TDestObject dest)
        {

            foreach (var destField in
                typeof(TDestObject).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                var sourceField =
                    typeof(TSourceObject).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Where(p => p.Name == destField.Name)
                    .FirstOrDefault();

                if (sourceField != null)
                {
                    var type = destField.PropertyType.Name;
                    var sourceValue = sourceField.GetValue(source, null);

                    switch (type)
                    {
                        case "String":
                            destField.SetValue(dest, sourceValue.ToString(), null);
                            break;
                        case "Boolean":
                            bool t;
                            if (bool.TryParse(sourceValue.ToString(), out t))
                            {
                                destField.SetValue(dest, t, null);
                            }
                            break;
                        case "Int32":
                            int inte;
                            if (int.TryParse(sourceValue.ToString(), out inte))
                            {
                                destField.SetValue(dest, inte, null);
                            }
                            break;
                        case "Double":
                            double dw;
                            if (double.TryParse(sourceValue.ToString(), out dw))
                            {
                                destField.SetValue(dest, dw, null);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public TDestObject Mapping(TSourceObject source)
        {
            TDestObject dest = new TDestObject();
            MatchField(source, dest);

            return dest;
        }



    }
}
