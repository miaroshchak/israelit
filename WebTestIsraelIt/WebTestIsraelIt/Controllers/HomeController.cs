﻿using Microsoft.AspNetCore.Mvc;
using WebTestIsraelIt.Mapping;
using WebTestIsraelIt.Models;

namespace WebTestIsraelIt.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {

        public HomeController()
        {
        }

        #region API

        [HttpGet]
        public JsonResult Get()
        {
            var source = CreateExampleSourceObject();

            var mapper = new Mapper<SourceObject, DestinationObject>();
            DestinationObject destination = mapper.Mapping(source);

            return new JsonResult(destination);
        }

        #endregion


        #region Methods

        private SourceObject CreateExampleSourceObject()
        {
            var src = new SourceObject()
            {
                Bool = true,
                Byte = 5,
                Char = 'A',
                Decimal = 10000,
                Double = 20000,
                Float = 30000,
                Int = 40000,
                Long = -50000,
                SByte = -5,
                Short = -6000,
                UInt = 70000,
                ULong = 50000,
                UShort = 6000,
                String = "Some text"
            };

            return src;
        }

        #endregion

    }
}