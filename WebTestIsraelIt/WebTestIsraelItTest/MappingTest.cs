using NUnit.Framework;
using WebTestIsraelIt.Mapping;
using WebTestIsraelIt.Models;

namespace WebTestIsraelItTest
{
    public class MappingTest
    {

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void MappingTestSourceToDestination()
        {
            var mapper = new Mapper<SourceObject, DestinationObject>();

            //arrange
            SourceObject src = new SourceObject()
            {
                Bool = true,
                Byte = 5,
                Char = 'A',
                Decimal = 10000,
                Double = 20000,
                Float = 30000,
                Int = 40000,
                Long = -50000,
                SByte = -5,
                Short = -6000,
                UInt = 70000,
                ULong = 50000,
                UShort = 6000,
                String = "Some text"
            };

            DestinationObject expected = new DestinationObject()
            {
                Bool = true.ToString(),
                Byte = 5.ToString(),
                Char = 'A'.ToString(),
                Decimal = 10000.ToString(),
                Double = 20000.ToString(),
                Float = 30000.ToString(),
                Int = 40000.ToString(),
                Long = (-50000).ToString(),
                SByte = (-5).ToString(),
                Short = (-6000).ToString(),
                UInt = 70000.ToString(),
                ULong = 50000.ToString(),
                UShort = 6000.ToString(),
                String = "Some text"
            };

            //act
            var actual = mapper.Mapping(src);


            //assert
            Assert.AreEqual(expected.Bool, actual.Bool);
            Assert.AreEqual(expected.Byte, actual.Byte);
            Assert.AreEqual(expected.Char, actual.Char);
            Assert.AreEqual(expected.Decimal, actual.Decimal);
            Assert.AreEqual(expected.Double, actual.Double);
            Assert.AreEqual(expected.Float, actual.Float);
            Assert.AreEqual(expected.Int, actual.Int);
            Assert.AreEqual(expected.Long, actual.Long);
            Assert.AreEqual(expected.SByte, actual.SByte);
            Assert.AreEqual(expected.Short, actual.Short);
            Assert.AreEqual(expected.UInt, actual.UInt);
            Assert.AreEqual(expected.ULong, actual.ULong);
            Assert.AreEqual(expected.UShort, actual.UShort);
            Assert.AreEqual(expected.String, actual.String);
        }

        [Test]
        public void MappingTestOtherSourceToOtherDestination() 
        {
            var mapper = new Mapper<OtherSourceObject, OtherDestinationObject>();

            //arrange
            OtherSourceObject source = new OtherSourceObject() 
            { 
                Age = 35,
                WorkExperience = 3
            };

            OtherDestinationObject expected = new OtherDestinationObject() 
            {
                Age = 35,
                WorkExperience = 3
            };

            //act
            var actual = mapper.Mapping(source);

            //assert
            Assert.AreEqual(expected.Age, actual.Age);
            Assert.AreEqual(expected.WorkExperience, actual.WorkExperience);
        }
    }
}